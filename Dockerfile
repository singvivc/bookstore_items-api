FROM golang:1.12.13
ENV REPO_URL=gitlab.com/singvivc/bootstore_items-api

ENV GOPATH=/app
ENV APP_PATH=$GOPATH/src/$REPO_URL

ENV WORKPATH=$APP_PATH/src
COPY src $WORKPATH
WORKDIR $WORKPATH

RUN go build -o items-api .

# EXPOSE Port 8081 to the world
EXPOSE 8081
CMD ["./items-api"]