package client

import (
	"context"
	"fmt"
	"time"

	"github.com/olivere/elastic"
	"gitlab.com/singvivc/bookstore_utils-go/logger"
)

var Client elasticClientInterface = &elasticClient{}

type elasticClientInterface interface {
	setElasticClient(*elastic.Client)
	Index(string, string, interface{}) (*elastic.IndexResponse, error)
	Get(string, string, string) (*elastic.GetResult, error)
	Search(string, elastic.Query) (*elastic.SearchResult, error)
}

type elasticClient struct {
	client *elastic.Client
}

func Init() {
	log := logger.GetLogger()
	client, err := elastic.NewClient(elastic.SetURL("http://127.0.0.1:9200"),
		elastic.SetHealthcheckInterval(10*time.Second),
		elastic.SetErrorLog(log), elastic.SetInfoLog(log))
	if err != nil {
		panic(err)
	}
	Client.setElasticClient(client)
}

func (e *elasticClient) setElasticClient(client *elastic.Client) {
	e.client = client
}

func (e *elasticClient) Index(index string, docType string, document interface{}) (*elastic.IndexResponse, error) {
	ctx := context.Background()
	result, err := e.client.Index().Index(index).Type(docType).BodyJson(document).Do(ctx)
	if err != nil {
		logger.Error(fmt.Sprintf("Error occurred when trying to index document in %s", index), err)
		return nil, err
	}
	return result, nil
}

func (e *elasticClient) Get(index string, docType string, id string) (*elastic.GetResult, error) {
	ctx := context.Background()
	result, err := e.client.Get().Index(index).Type(docType).Id(id).Do(ctx)
	if err != nil {
		logger.Error(fmt.Sprintf("Error when trying to get document by %s", id), err)
		return nil, err
	}
	return result, nil
}

func (e *elasticClient) Search(index string, query elastic.Query) (*elastic.SearchResult, error) {
	ctx := context.Background()
	result, err := e.client.Search(index).Query(query).RestTotalHitsAsInt(true).Do(ctx)
	if err != nil {
		logger.Error(fmt.Sprintf("Error while trying to search for the document in index %s", index), err)
		return nil, err
	}
	return result, nil
}
