package controller

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/singvivc/bookstore_items-api/src/domain/item"
	"gitlab.com/singvivc/bookstore_items-api/src/domain/itemquery"
	"gitlab.com/singvivc/bookstore_items-api/src/domain/service"
	"gitlab.com/singvivc/bookstore_items-api/src/utils"
	"gitlab.com/singvivc/bookstore_oauth-go/oauth"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
	"io/ioutil"
	"net/http"
	"strings"
)

var (
	itemService                            = service.Service
	ItemController itemControllerInterface = &itemController{}
)

type itemControllerInterface interface {
	Create(http.ResponseWriter, *http.Request)
	Get(http.ResponseWriter, *http.Request)
	Search(http.ResponseWriter, *http.Request)
}

type itemController struct{}

func (c *itemController) Create(writer http.ResponseWriter, request *http.Request) {
	if err := oauth.Authenticate(request); err != nil {
		utils.RespondWithError(writer, err)
		return
	}
	if oauth.GetCallerId(request) == 0 {
		restErr := errors.UnauthorizedError("Request not authorized")
		utils.RespondWithError(writer, restErr)
		return
	}
	requestBody, err := ioutil.ReadAll(request.Body)
	if err != nil {
		restErr := errors.BadRequestError("Invalid request body")
		utils.RespondWithError(writer, restErr)
		return
	}
	defer request.Body.Close()
	var itemRequest item.Item
	if err := json.Unmarshal(requestBody, &itemRequest); err != nil {
		restErr := errors.BadRequestError("Invalid json body")
		utils.RespondWithError(writer, restErr)
		return
	}
	itemRequest.Seller = oauth.GetCallerId(request)
	result, createdErr := itemService.Create(itemRequest)
	if createdErr != nil {
		utils.RespondWithError(writer, createdErr)
	}
	utils.JsonResponse(writer, http.StatusCreated, result)
}

func (c *itemController) Get(writer http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	itemId := strings.TrimSpace(vars["id"])
	item, err := itemService.Get(itemId)
	if err != nil {
		utils.RespondWithError(writer, err)
		return
	}
	utils.JsonResponse(writer, http.StatusOK, item)
}

func (c *itemController) Search(writer http.ResponseWriter, request *http.Request) {
	bytes, err := ioutil.ReadAll(request.Body)
	if err != nil {
		apiError := errors.BadRequestError("Invalid request body")
		utils.RespondWithError(writer, apiError)
		return
	}
	defer request.Body.Close()
	var query itemquery.ItemQuery
	if err := json.Unmarshal(bytes, &query); err != nil {
		apiError := errors.BadRequestError("Invalid request body")
		utils.RespondWithError(writer, apiError)
		return
	}
	items, searchError := itemService.Search(query)
	if searchError != nil {
		utils.RespondWithError(writer, searchError)
		return
	}
	utils.JsonResponse(writer, http.StatusOK, items)
}