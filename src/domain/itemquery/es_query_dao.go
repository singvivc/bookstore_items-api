package itemquery

import "github.com/olivere/elastic"

func (q ItemQuery) Build() elastic.Query {
	criteria := make([]elastic.Query, 0)
	for _, eq := range q.Equals {
		criteria = append(criteria, elastic.NewMatchQuery(eq.Field, eq.Value))
	}
	query := elastic.NewBoolQuery()
	query.Must(criteria...)
	return query
}