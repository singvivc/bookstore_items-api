package service

import (
	"gitlab.com/singvivc/bookstore_items-api/src/domain/item"
	"gitlab.com/singvivc/bookstore_items-api/src/domain/itemquery"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
)

var Service itemServiceInterface = &itemService{}

type itemServiceInterface interface {
	Create(item.Item) (*item.Item, errors.RestError)
	Get(string) (*item.Item, errors.RestError)
	Search(itemquery.ItemQuery) ([]item.Item, errors.RestError)
}

type itemService struct{}

func (s *itemService) Create(item item.Item) (*item.Item, errors.RestError) {
	if err := item.Save(); err != nil {
		return nil, err
	}
	return &item, nil
}

func (s *itemService) Get(id string) (*item.Item, errors.RestError) {
	item := item.Item{Id: id}
	if err := item.Get(); err != nil {
		return nil, err
	}
	return &item, nil
}

func (s *itemService) Search(query itemquery.ItemQuery) ([]item.Item, errors.RestError) {
	itemDao := item.Item{}
	return itemDao.Search(query)
}