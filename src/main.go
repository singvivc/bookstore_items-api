package main

import "gitlab.com/singvivc/bookstore_items-api/src/app"

func main() {
	app.StartBookStoreItemApplication()
}
