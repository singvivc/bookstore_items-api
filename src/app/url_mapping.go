package app

import (
	"gitlab.com/singvivc/bookstore_items-api/src/controller"
	"net/http"
)

func mapRouterUrls() {
	healthController := controller.HealthController
	router.HandleFunc("/health", healthController.Health).Methods(http.MethodGet)

	itemController := controller.ItemController
	// Create new Item
	router.HandleFunc("/items", itemController.Create).Methods(http.MethodPost)
	router.HandleFunc("/items/{id}", itemController.Get).Methods(http.MethodGet)
	router.HandleFunc("/items/search", itemController.Search).Methods(http.MethodPost)
}
