package app

import (
	"github.com/gorilla/mux"
	"gitlab.com/singvivc/bookstore_items-api/src/client"
	"net/http"
	"time"
)

var router = mux.NewRouter()

func StartBookStoreItemApplication() {
	client.Init()
	mapRouterUrls()
	server := &http.Server{Handler: router, Addr: "localhost:8082",
		WriteTimeout: 500 * time.Millisecond, ReadTimeout: 15 * time.Second,
		IdleTimeout: time.Second * 60}
	if err := server.ListenAndServe(); err != nil {
		panic(err)
	}
}
