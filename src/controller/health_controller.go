package controller

import (
	"gitlab.com/singvivc/bookstore_items-api/src/utils"
	"net/http"
)

var HealthController healthControllerInterface = &healthController{}

type healthControllerInterface interface {
	Health(http.ResponseWriter, *http.Request)
}

type healthController struct{}

type health struct {
	Status string `json:"status"`
}

func (c *healthController) Health(writer http.ResponseWriter, request *http.Request) {
	utils.JsonResponse(writer, http.StatusOK, &health{Status: "Ok"})
}
