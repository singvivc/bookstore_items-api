package item

import (
	"encoding/json"
	errors2 "errors"
	"fmt"
	"gitlab.com/singvivc/bookstore_items-api/src/client"
	"gitlab.com/singvivc/bookstore_items-api/src/domain/itemquery"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
)

const (
	indexItem = "items"
	docType   = "items"
)

var elasticClient = client.Client

func (i *Item) Save() errors.RestError {
	result, err := elasticClient.Index(indexItem, docType, i)
	if err != nil {
		return errors.InternalServerError("Error when trying to save item", errors2.New("database error"))
	}
	i.Id = result.Id
	return nil
}

func (i *Item) Get() errors.RestError {
	itemId := i.Id
	result, err := elasticClient.Get(indexItem, docType, itemId)
	if err != nil {
		return errors.InternalServerError("Error occurred when trying to get the document",
			errors2.New("database error"))
	}
	if !result.Found {
		return errors.NotFoundError(fmt.Sprintf("Unable to find document for the given id %s", i.Id))
	}
	bytes, err := result.Source.MarshalJSON()
	if err != nil {
		return errors.InternalServerError("Error when trying to parse the response")
	}
	if err := json.Unmarshal(bytes, i); err != nil {
		return errors.InternalServerError("Error when trying to parse the response")
	}
	i.Id = itemId
	return nil
}

func (i *Item) Search(query itemquery.ItemQuery) ([]Item, errors.RestError) {
	result, err := elasticClient.Search(indexItem, query.Build())
	if err != nil {
		return nil, errors.InternalServerError("Error while searching for the document", errors2.New("database error"))
	}
	items := make([]Item, result.TotalHits())
	for index, hit := range result.Hits.Hits {
		bytes, _ := hit.Source.MarshalJSON()
		var item Item
		if err := json.Unmarshal(bytes, &item); err != nil {
			return nil, errors.InternalServerError("error when trying to parse the response", errors2.New("database error"))
		}
		item.Id = hit.Id
		items[index] = item
	}
	return items, nil
}
