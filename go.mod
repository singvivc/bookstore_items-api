module gitlab.com/singvivc/bookstore_items-api

go 1.14

require (
	github.com/gorilla/mux v1.7.4
	github.com/mailru/easyjson v0.7.1 // indirect
	github.com/olivere/elastic v6.2.30+incompatible
	gitlab.com/singvivc/bookstore_oauth-go v0.0.0-20200420192607-8306d45b21f4
	gitlab.com/singvivc/bookstore_utils-go v0.0.0-20200420191921-591738e9b2e5
)
