package utils

import (
	"encoding/json"
	"gitlab.com/singvivc/bookstore_utils-go/errors"
	"net/http"
)

func JsonResponse(writer http.ResponseWriter, status int, body interface{}) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(status)
	json.NewEncoder(writer).Encode(body)
}

func RespondWithError(writer http.ResponseWriter, restError errors.RestError) {
	JsonResponse(writer, restError.Status(), restError)
}
